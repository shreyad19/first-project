##########################
# Main (Terraform Setup) #
##########################
terraform {
  backend "s3" {
    bucket         = "terraform-docker-tfstate"  #s3bucketname#
    key            = "shreyad/terraform.tfstate" #IAMkey-always should be .tfstate#
    region         = "us-east-1"                 #alltheservicesshouldbecreatedinsameregion#
    encrypt        = true
    dynamodb_table = "terraform-docker" #dynamodbtableandprimarykeyshouldbeLockID#
  }
}


provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}
locals {
  common_tags = {
    Environment = terraform.workspace
    ManagedBy   = "Terraform" #managedbyterraform
  }
}
#######################
# Server #
##########
data "aws_ami" "server_name" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"]
}
resource "aws_instance" "server" {
  ami           = data.aws_ami.server_name.id
  instance_type = var.instance_type
  tags = merge(
    local.common_tags,
    map("Name", var.server_name)
  )
}


